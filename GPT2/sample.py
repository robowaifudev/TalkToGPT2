'''
    code by TaeHwan Jung(@graykode)
    Original Paper and repository here : https://github.com/openai/gpt-2
    GPT2 Pytorch Model : https://github.com/huggingface/pytorch-pretrained-BERT
'''
import torch
import torch.nn.functional as F
from tqdm import trange

def top_k_logits(logits, k, filter_value=-float('inf')):
    if k == 0:
        return logits
    values, _ = torch.topk(logits, k)
    min_values = values[:, -1]
    return torch.where(logits < min_values, torch.ones_like(logits, dtype=logits.dtype) * filter_value, logits)

def top_p_logits(logits, p, filter_value=-float('inf')):
    if p == 0:
        return logits
    sorted_logits, sorted_indices = torch.sort(logits, descending=True)
    cumulative_probs = torch.cumsum(F.softmax(sorted_logits, dim=-1), dim=-1)

    # Remove tokens with cumulative probability above the threshold
    sorted_indices_to_remove = cumulative_probs > p
    # Shift the indices to the right to keep also the first token above the threshold
    sorted_indices_to_remove[1:] = sorted_indices_to_remove[:-1].clone()
    sorted_indices_to_remove[0] = 0

    indices_to_remove = sorted_indices[sorted_indices_to_remove]
    logits[indices_to_remove] = filter_value
    return logits

def sample_sequence(model, length, start_token=None, end_tokens=None, batch_size=None, context=None, temperature=1, top_k=0, top_p=0.0, device='cuda', sample=True, verbose=True):
    if start_token is None:
        assert context is not None, 'Specify exactly one of start_token and context!'
        context = torch.tensor(context, device=device, dtype=torch.long).unsqueeze(0).repeat(batch_size, 1)
    else:
        assert context is None, 'Specify exactly one of start_token and context!'
        context = torch.full((batch_size, 1), start_token, device=device, dtype=torch.long)
    prev = context
    output = context
    past = None
    if end_tokens:
        for i in range(len(end_tokens)):
            if type(end_tokens[i]) == list:
                end_tokens[i] = torch.tensor(end_tokens[i], device=device)
            else:
                end_tokens[i] = end_tokens[i].clone().detach()
    if verbose:
        range_mode = trange
    else:
        range_mode = range
    with torch.no_grad():
        for i in range_mode(length):
            logits, past = model(prev, past=past)
            tpast = []
            for arr in past:
                tpast.append(arr[:,:,:,-1024:,:])
            past = tpast
            #print(len(past), past[0].shape)
            logits = logits[:, -1, :].type(torch.double) / temperature
            logits = top_k_logits(logits, k=top_k)
            logits = top_p_logits(logits, p=top_p)
            log_probs = F.softmax(logits, dim=-1)
            if sample:
                #print(log_probs.shape)
                prev = torch.multinomial(log_probs, num_samples=1)
            else:
                _, prev = torch.topk(log_probs, k=1, dim=-1, device=device)
            #print('prev', prev)
            #print('end_tokens', end_tokens)
            if end_tokens:
                end = False
                for i in range(len(end_tokens)):
                    #print(context.size(1))
                    if len(output.squeeze(0)[context.size(1):][-end_tokens[i].size(0):]) != end_tokens[i].size(0):
                        continue
                    if torch.any(output.squeeze(0)[len(context):][-end_tokens[i].size(0):] != end_tokens[i]): continue
                    #print(output.squeeze(0)[context.size(1):])
                    #print(output.squeeze(0)[context.size(1):][-end_tokens[i].size(0):])
                    #print("[End token]", end_tokens[i])
                    output = output[:,:-end_tokens[i].size(0)]
                    end = True
                if end: break
            output = torch.cat((output, prev), dim=1)
            if output.size(1) >= 1024: break
    return output
