# TalkToGPT2 (formerly TalkToWaifu)
Simple chatbot using GPT2 in PyTorch, forked from [gpt-2-Pytorch](https://github.com/graykode/gpt-2-Pytorch) by graykode. Waifus not included.

| Section | Description |
|-|-|
| [How to Install](#installation) | How to install the package |
| [Quick Start](#quick-start-for-talktogpt2) | Quick start for TalkToGPT2 |
| [Text Generation](#quick-start-for-gpt2) | Text generation with GPT2 |
| [Training](#training) | Training your own models |

## Installation
### Requirements
* [Python 3](https://www.python.org/downloads) and [pip](https://pypi.org/project/pip/) (regex tqdm ansiwrap)
* [PyTorch](pytorch.org/)
* [Cuda](https://developer.nvidia.com/cuda-downloads) (optional, only if you want it to run fast)

### Install
```shell
git clone https://gitlab.com/kokubunji/TalkToGPT2
pip3 install regex tqdm ansiwrap
```
### Download Model
```shell
cd TalkToGPT2
wget https://s3.amazonaws.com/models.huggingface.co/bert/gpt2-medium-pytorch_model.bin
```

## Quick start for TalkToGPT2
**Keep in mind**: GPT2 requires at least 100 words to generate remotely meaningful text.
```shell
python3 gpt2waifu.py --prompt "How much wood could a woodchuck chuck if a woodchuck could chuck wood?" --init "It depends on the quantum fluxuation of" --chattiness 3
```
**Sample output**:
```
You: How much wood could a woodchuck chuck if a woodchuck could chuck wood?
Ai: It depends on the quantum fluxuation of the woodchuck, and I think it's probably not as high as it should be.
Ai: I would say that the average woodchuck chuck is just an average of about 1,000,000,000 threads, which is really low.
Ai: I think that this can be considered normal woodchuck chuck usage.
You: How can we upgrade our woodchucks?
Ai: In this case, I would recommend using a woodchuck chuck of any size.
Ai: It's very easy to upgrade your woodchucks if you do it right.
```
### Arguments
* `--waifu` chatbot vanity name
* `--name` user vanity name
* `--gpt2-waifu` chatbot name used for GPT-2 generation
* `--gpt2-name` user name used for GPT-2 generation
* `--prompt` user's initial message to the chatbot
* `--init` initialize chatbot's first response to continue generating from

* `--top-p` nucleus sampling, takes a cumulative distribution and cuts off as soon as the cdf exceeds p
* `--top-k` trash sampling, picks the top k tokens, set to 0 by default to turn it off
* `--min-temperature` minimum temperature
* `--max-temperature` maximum temperature
* `--temperature-curve` temperature exponent factor of the random temperature sampling
* `--no-wrap` turn text wrapping off
* `--wrap-width` set text wrapping width
* `--chattiness` the max amount of lines the chatbot can reply with (default=3) (**warning: not complete**)
* `--model` GPT2 model to use (supported models: distilgpt2, gpt2, gpt2-medium, gpt2-large, gpt2-xl)
* `--gpu` set to 0 to use the GPU, -1 for cpu (default)
* `--quiet` turn off warning messages for too little tokens and thought loops
* `--help` for more options and in development features

## Quick start for GPT2
**Keep in mind**: GPT2 requires at least 100 words to generate remotely meaningful text. The more the better.
```shell
python3 gpt2.py --samples 4 --text "This has to be one of the biggest breakthroughs in deep learning and AI so far, but some disagree saying that it"
```
**Sample output**:
```
=================================== SAMPLE 3 ===================================
This has to be one of the biggest breakthroughs in deep learning and AI so far,
but some disagree saying that it's too early to say whether that will be the
case.  One of the biggest hurdles to overcome is the fact that many data
scientists say that deep learning is too complex to fully understand, and many
people have a hard time understanding it.  And while that is an area where deep
learning isn't quite there yet, many of the researchers who have come out of
MIT's OpenCourseWare Program say that many of the techniques that are currently
being used to create new problems in deep learning will give researchers a
chance in the future to create new problems in the field.  "This is one of the
biggest breakthroughs in deep learning and AI so far, but some disagree saying
that it's too early to say whether that will be the case," said Dr. Joseph
Sutter, a leading researcher in deep learning and the head of the Institute for
Advanced Learning in the Department of Computer Science at MIT.
```
### Arguments
* `--samples` generation attempts
* `--text` prompt to start generating from
* `--seed` text file to start generating from
* `--length` amount of words to generate
* `--min-temperature` 
* `--max-temperature` 
* `--temperature-curve` 
* `--no-wrap` turn text wrapping off
* `--wrap-width` set text wrapping width

## Training

Clone into [Transformers](https://github.com/huggingface/transformers/) from the same parent directory as TalkToWaifu and install it:
```shell
git clone https://github.com/huggingface/transformers/
cd transformers
python setup.py install
```

Change directory back to TalkToWaifu and link the model into the model folder:
```shell
cd ../TalkToWaifu
ln -s ../gpt2-medium-pytorch_model.bin gpt2-medium/pytorch_model.bin
```
Prepare a `train.txt` file for training and `test.txt` file for testing. Use `<|endoftext|>` tokens to mark the end of documents. If you're lazy, you can use the training file as a test file, but it won't give you any idea how well it's doing or if it's overfitting and becoming ruined.

To train:
```shell
# train.sh usage: model_path output_path training_file test_file
train.sh ./gpt2-medium train train.txt test.txt
```

**Warning**: Training GPT2 requires a lot of memory and processing power. My PC with 16 GB of memory can only handle fine-tuning `gpt2-medium` with a block size of 384.

### To train even further beyond
You can modify the block size and learning rate by editing the `train.sh` script.

If you want to train other GPT2 models, you will need to download the proper `config.json`, `vocab.json`, `modelcard.json` and `merges.txt` from [huggingface.co](https://huggingface.co/models?filter=pytorch&search=gpt2) and make a model folder for it with the `pytorch_model.bin` file.

If you don't use a path for the model path, for example just `gpt2-large`, the script might download the necessary files automatically but I haven't tested this myself.
