import sys

class NoColors:
    RED = ""
    YELLOW = ""
    GREEN = ""
    BLUE = ""
    BOLD = ""
    ENDC = ""
    USER = ""
    WAIFU = ""

class Colors:
    RED = "\033[91m"
    YELLOW = "\033[93m"
    GREEN = "\033[92m"
    BLUE = "\033[94m"
    BOLD = "\033[1m"
    ENDC = "\033[0m"
    USER = BOLD + GREEN
    WAIFU = BOLD + RED
