'''
    code by TaeHwan Jung(@graykode), modifications by kokubunji
    Original Paper and repository here : https://github.com/openai/gpt-2
    GPT2 Pytorch Model : https://github.com/huggingface/pytorch-pretrained-BERT
'''
import os
import random
import torch
import argparse

from GPT2.model import GPT2LMHeadModel
from GPT2.utils import load_weight
from GPT2.config import GPT2Config
from GPT2.sample import sample_sequence
from GPT2.encoder import get_encoder

from wrap import wrap_print
from colors import *

import config
import math

def temperature_curve_sample(min_temperature, max_temperature, exponent):
    return (max_temperature-min_temperature)*random.random()**exponent+min_temperature

def text_generator(args):
    gpt2_config, model_file = config.config[args.model]
    
    if not os.path.exists(model_file):
        print("Model not found, please download {}".format(args.model))
        print("https://s3.amazonaws.com/models.huggingface.co/bert/{}-pytorch_model.bin".format(args.model))
        raise SystemExit
    
    if not args.quiet:
        print(args)

    seed = random.randint(0, 2147483647)
    torch.random.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    
    device = torch.device("cuda" if torch.cuda.is_available() and args.gpu >= 0 else "cpu")
    print("Using device {}".format(device))
    
    state_dict = torch.load(model_file, map_location=device)
    if args.verbose:
        print(args)

    # Load Model
    enc = get_encoder()
    #config = GPT2Config()
    gpt2_config = GPT2Config(**gpt2_config)    
    model = GPT2LMHeadModel(gpt2_config)
    model = load_weight(model, state_dict)
    model.to(device)
    model.eval()
    
    if args.length == -1:
        args.length = gpt2_config.n_ctx // 2
    elif args.length > gpt2_config.n_ctx:
        pass #raise ValueError("Can't get samples longer than window size: %s" % config.n_ctx)
    
    if args.seed:
        text = open(args.seed).read()
    else:
        text = args.text
    context = text.replace(">", "").replace("<", "").replace("\n","\n\n").strip()
    #context = args.text.replace(">", "\n")
    if args.verbose: print(context)
    context_tokens = enc.encode(context)
    
    # text wrapping
    if args.no_wrap:
        wrap_width = 0
    else:
        wrap_width = args.wrap_width
    
    generated = 0
    for _ in range(args.samples):
        temperature = temperature_curve_sample(args.min_temperature, args.max_temperature, args.temperature_curve)
        out = sample_sequence(
            model=model, length=args.length,
            context=context_tokens  if not  args.unconditional else None,
            start_token=enc.encoder['<|endoftext|>'] if args.unconditional else None,
            end_tokens=[[enc.encoder["<|endoftext|>"]]],
            batch_size=1,
            temperature=temperature,
            top_k=args.top_k,
            top_p=args.top_p,
            device=device,
            verbose=not args.quiet
        )
        out = out[:, len(context_tokens):].tolist()
        text = enc.decode(out[0])
        generated += 1
        if not args.quiet:
            print("=" * 35 + " SAMPLE " + str(generated) + " " + "=" * 35)
        wrap_print(C_GREEN + context + C_ENDC + text, wrap_width)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--text", type=str, required=True)
    parser.add_argument("--seed", type=str)
    parser.add_argument("--quiet", type=bool, default=False)
    parser.add_argument("--samples", type=int, default=1)
    parser.add_argument("--wrap-width", type=int, default=80)
    parser.add_argument('--no-wrap', action='store_true')
    parser.add_argument('--unconditional', action='store_true', help='if true, unconditional generation.')
    parser.add_argument("--length", type=int, default=300, help="amount of words to generate")
    parser.add_argument("--history-length", type=int, default=1000, help="maximum context history length in gpt-2 tokens")
    parser.add_argument("--model", help="gpt2 model", default="gpt2-medium", choices=["distilgpt2", "gpt2", "gpt2-medium", "gpt2-large", "gpt2-xl"])
    parser.add_argument("--top-k", type=int, default=0)
    parser.add_argument("--min-temperature", "-t", type=float, default=0.6)
    parser.add_argument("--max-temperature", "-mt", type=float, default=1.0)
    parser.add_argument("--temperature-curve", "-tc", type=float, default=math.e)
    parser.add_argument("--top-p", type=float, default=0.8)
    parser.add_argument("--gpu", type=int, default=-1)
    parser.add_argument("--verbose", "-v", action="store_true")
    args = parser.parse_args()
    text_generator(args)
