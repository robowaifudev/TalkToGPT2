import os
import random
import torch
import math
import sys
import re

from GPT2.model import GPT2LMHeadModel
from GPT2.utils import load_weight
from GPT2.config import GPT2Config
from GPT2.sample import sample_sequence
from GPT2.encoder import get_encoder
import Levenshtein

import model_config
from wrap import wrap_print
from default_settings import DEFAULT
from colors import NoColors, Colors

__version__ = "0.3-alpha"

def temperature_curve_sample(min_temperature, max_temperature, exponent):
    return (max_temperature-min_temperature)*random.random()**exponent+min_temperature

MIN_RESPONSE_LENGTH = 32
random_responses = [
    "Sorry, I'm not sure what to say.",
    "Sorry, I'm a bit confused.",
    "Sorry, this conversation is a bit too repetitive for me.",
    "Sorry, can you say that another way?",
    "I'm sorry, this is random, but do you know if computers dream of electric sheep?",
    "I'm sorry, but you might have to reset me."
    "I'm sorry, I'm not a good chatbot."
]

def thought_loop_check(output, last_response, prev_last_response, user_input, distance):
    if Levenshtein.distance(output, last_response) < distance:
        #print(1)
        return True
    if Levenshtein.distance(output, prev_last_response) < distance:
        #print(2)
        return True
    if Levenshtein.distance(output, user_input) < max(distance, 4):
        #print(3)
        return True
    d = min(len(output), len(last_response))
    if Levenshtein.distance(output[:d], last_response[:d]) < min(d, distance):
        #print(4)
        return True
    d = min(len(output), len(prev_last_response))
    if Levenshtein.distance(output[:d], prev_last_response[:d]) < min(d, distance):
        #print(5)
        return True
    #d = min(len(output), len(user_input))
    #if Levenshtein.distance(output[:d], user_input[:d]) < min(d, distance): return True
    if output.find(last_response) >= 0:
        #print(6)
        return True
    if output.find(prev_last_response) >= 0:
        #print(7)
        return True
    if len(user_input) > 0 and output.find(user_input) >= 0:
        #print(8)
        return True
    if last_response.find(output) >= 0:
        #print(9)
        return True
    if prev_last_response.find(output) >= 0:
        #print(10)
        return True
    if user_input.find(output) >= 0:
        #print(11)
        return True
    return False

class Config:
    def __init__(self, config):
        self.__dict__.update(config)
    
    def update(self, d):
        self.__dict__.update(d)
    
    def __getitem__(self, key):
        return self.__dict__[key]
    
    def __iter__(self):
        return iter(self.__dict__)
    
    def __repr__(self):
        return repr(self.__dict__)

def main(config):
    if config.verbose:
        for key in config:
            print(f"{key}: {config[key]}")
    gpt2_config, model_file = model_config.config[config.model]
    
    if config.no_colors:
        C = NoColors
    else:
        C = Colors
    
    if not os.path.exists(model_file):
        print("Model not found, please download {}".format(config.model))
        print("https://s3.amazonaws.com/models.huggingface.co/bert/{}-pytorch_model.bin".format(config.model))
        raise SystemExit
    
    device = torch.device("cuda" if torch.cuda.is_available() and config.gpu >= 0 else "cpu")
    print("Welcome to TalkToWaifu v{}, using {} on {}".format(__version__, config.model, device))
    
    save_settings = {}
    # TODO make function
    if config.gpt2_name is None:
        name = input(C.YELLOW + f"What is your name? [{C.ENDC}{DEFAULT['display_name']}{C.YELLOW}] " + C.ENDC)
        if name == "":
            name = DEFAULT['display_name']
        config.gpt2_name = name
        config.display_name = name
        remember = input(C.YELLOW + f"Remember this name: {C.ENDC}{name}{C.YELLOW}? [{C.ENDC}Y/n{C.YELLOW}] " + C.ENDC)
        if remember.lower() == "y" or remember.lower() == "":
            save_settings["gpt2_name"] = name
            save_settings["display_name"] = name
    
    if config.gpt2_waifu is None:
        name = input(C.YELLOW + f"What is your waifu's name? [{C.ENDC}{DEFAULT['display_waifu']}{C.YELLOW}] " + C.ENDC)
        if name == "":
            name = DEFAULT["display_waifu"]
        config.gpt2_waifu = name
        config.display_waifu = name
        remember = input(C.YELLOW + f"Remember this name: {C.ENDC}{name}{C.YELLOW}? [{C.ENDC}Y/n{C.YELLOW}] " + C.ENDC)
        if remember.lower() == "y" or remember.lower() == "":
            save_settings["gpt2_waifu"] = name
            save_settings["display_waifu"] = name
    
    if len(save_settings) > 0:        
        saved_config = json.load(open("config.json"))
        for key in save_settings:
            saved_config[key] = save_settings[key]
        json.dump(saved_config, open("config.json", "w"), sort_keys=True, indent=4)
        print("Settings saved to config.json")
    
    # load model
    state_dict = torch.load(model_file, map_location=device)

    seed = random.randint(0, 2147483647)
    torch.random.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    if config.verbose:
        print("Seed:", seed)
    
    # Load Model
    
    enc = get_encoder()
    gpt2_config = GPT2Config(**gpt2_config)
    model = GPT2LMHeadModel(gpt2_config)
    model = load_weight(model, state_dict)
    model.to(device)
    model.eval()
    
    torch.cuda.set_device(config.gpu)
    
    max_length = min(config.history_length, gpt2_config.n_ctx)
    if config.max_response_length == -1:
        config.max_response_length = max_length // 2
    elif config.max_response_length > max_length-MIN_RESPONSE_LENGTH:
        raise ValueError(f"Can't get samples longer than window size - minimum response length: {max_length} - {MIN_RESPONSE_LENGTH}")
    
    if config.seed:
        if config.rp:
            seed_text = open(config.seed).read().strip()
            seed_text = re.sub("\[you\]", config.gpt2_name, seed_text, flags=re.I)
            seed_text = re.sub("\[waifu\]", config.gpt2_waifu, seed_text, flags=re.I)
            #msg = seed_text
            gpt2_text = seed_text + "\n\n"
            #if config.debug: print(C.YELLOW + gpt2_text + C.ENDC)
            print(C.USER + seed_text + "\n" + C.ENDC)
            config.seed = None
        else:
            seed_text = open(config.seed).read().strip().replace(config.display_name + ":", config.gpt2_name + ":").replace(config.display_waifu + ":", config.gpt2_waifu + ":")
            seed_text = re.sub("\[you\]", config.gpt2_name, seed_text, flags=re.I)
            seed_text = re.sub("\[waifu\]", config.gpt2_waifu, seed_text, flags=re.I)
            #msg = seed_text
            gpt2_text = seed_text + "\n"
            #if config.debug: print(C.YELLOW + gpt2_text + C.ENDC)
            #print(seed_text.replace(config.gpt2_name, C.USER + config.display_name + C.ENDC).replace(config.gpt2_waifu, C.WAIFU + config.display_waifu + C.ENDC))
            output_seed = ""
            for line in seed_text.split('\n'):
                output_seed += line[:len(max(config.gpt2_waifu, config.gpt2_name))].replace(config.gpt2_name, C.USER + config.display_name + C.ENDC, 1).replace(config.gpt2_waifu, C.WAIFU + config.display_waifu + C.ENDC, 1) + line[len(max(config.gpt2_waifu, config.gpt2_name)):] + '\n'
            print(output_seed.strip())
            config.seed = None
    else:
        gpt2_text = ""
    if config.log: chatlog = open("chat.log", "a")
    
    prompt = config.prompt
    init = config.init
    
    # default gpt-2 to generate with waifu name
    if config.gpt2_waifu:
        gpt2_waifu = config.gpt2_waifu
    else:
        gpt2_waifu = config.display_waifu
    
    # for preventing truncated chat history messing up GPT-2
    if config.rp:
        valid_newlines = ['']
    else:
        valid_newlines = [
            config.gpt2_waifu + ": ",
            config.gpt2_name + ": "
        ]
    
    #print(valid_newlines)
    #for valid_newline in valid_newlines:
    #    print(enc.decode(valid_newline))
    
    if config.rp:
        end_tokens = [enc.encode("\n") + enc.encode("\n"), enc.encode("\n\n"), [enc.encoder['<|endoftext|>']]]
    else:
        end_tokens = [enc.encode("\n"), [enc.encoder['<|endoftext|>']]]
    #if config.debug:
    #    print(end_tokens)
    
    #outputs = {}
    last_response = "---"
    prev_last_response = "---"
    additional_temperature = 0.0
    while True:
        finish_sentence = False
        adventure = config.adventure
        #action_copycat = False
        if prompt:
            if config.rp:
                wrap_print(prompt, config.wrap_width)
            else:
                wrap_print(C.USER + config.display_name + C.ENDC + ": " + prompt, config.wrap_width)
            user_input = prompt
            prompt = None            
        else:
            if config.rp:
                user_input = input(C.USER)
                sys.stdout.write(C.ENDC)
            else:
                user_input = input(C.USER + config.display_name + C.ENDC + ": ")
            # TODO make commands
            if len(user_input) > 0:
                # removing this for now
                # if user_input.find("|") > 0: # /puppet forget what this does
                #     ind = user_input.find("|")
                #     puppet_text = user_input[ind+1:]
                #     outputs[puppet_text.lstrip()] = 1
                #     user_input = user_input[:ind]
                #     outputs[user_input.lstrip()] = 1
                #     if config.rp:
                #         gpt2_text += user_input + "\n"
                #         gpt2_text += puppet_text + "\n"
                #     else:
                #         gpt2_text += config.gpt2_name + ": " + user_input + "\n"
                #         gpt2_text += config.gpt2_waifu + ": " + puppet_text + "\n"
                #     continue
                # elif user_input.find("<>") > 0: # /puppet puppets the waifu with the rest of your text after the pipe
                #     ind = user_input.find("<>")
                #     init = user_input[ind+2:]
                #     user_input = user_input[:ind]
                # else:
                # TODO this needs to be replaced with commands
                if user_input[-1] == "+": # /question
                    init = random.choice(["How", "What", "How"])
                    user_input = user_input[:-1]
                elif user_input[-1] == "=": # /summarize
                    init = random.choice(["So it seems", "Then does that mean", "I think"])
                    user_input = user_input[:-1]
                elif user_input[-1] == "^": # /bigthink, intended for discussing ideas and brainstorming
                    init = random.choice([
                        "What if",
                        "I wonder if",
                        "I wonder why",
                        "I wonder whether",
                        "Perhaps we could",
                        "That would work if",
                        "That would work when",
                        "In what ways can we",
                        "In what ways",
                        "If it were possible",
                        "If you could",
                        "How would it be different if"
                    ])
                    user_input = user_input[:-1]
                elif user_input[-1] == "`": # /adventure toggle
                    adventure = adventure^True
                    user_input = user_input[:-1]
                elif user_input[-1] == ">": # /autocomplete
                    user_input = user_input[:-1]
                    finish_sentence = True
                    init = user_input
                # elif user_input[-1] == "<": # /autocomplete adventure mode?
                #     user_input = user_input[:-1]
                #     finish_sentence = True
                #     adventure = True
                # if len(user_input) > 0 and (user_input[0] == "*" or user_input[-1] == "*"): # /me copycat mode
                #     if init == "" and random.random() < config.action_copycat:
                #         action_copycat = True
        
        # if action_copycat:
        #     # TODO shit code doesn't make it easy to use a different init for each sample in adventure mode
        #     init = "*"
        
        if user_input != "":
            if config.rp:
                if finish_sentence:
                    gpt2_msg = user_input
                else:
                    gpt2_msg = user_input + "\n"
            else:
                if finish_sentence:
                    gpt2_msg = config.gpt2_name + ": " + user_input
                else:
                    gpt2_msg = config.gpt2_name + ": " + user_input + "\n"
        else:
            gpt2_msg = ""
        if config.log: chatlog.write(gpt2_msg)
        gpt2_text += gpt2_msg
        original_gpt2_text = gpt2_text
        original_init = init
        #outputs[user_input] = 1
        for r in range(1):
            # TODO: generate multiple possible responses to create training data
            gpt2_text = original_gpt2_text
            prefix = ""
            init = original_init
            for line in range(random.randint(1, config.chattiness)):
                if not finish_sentence:
                    if not config.rp:  
                        prefix += gpt2_waifu + ":"
                        if init != "":
                            prefix += " " + init
                    else:
                        if init != "":
                            prefix += init
                #else:
                #    gpt2_text += config.gpt2_name + ":"
                #    if init != "":
                #        gpt2_text += " " + user_input
                
                context_tokens = enc.encode(gpt2_text + prefix)[-config.history_length+config.max_response_length:]
                gpt2_text = enc.decode(context_tokens)
                
                # prevent truncated chat history messing up GPT-2
                valid_context = False
                while not valid_context:
                    for valid_newline in valid_newlines:
                        if gpt2_text[:len(valid_newline)] == valid_newline:
                            valid_context = True
                            break
                    if not valid_context:
                        try:
                            newline_ind = gpt2_text.find("\n")
                            if newline_ind >= 0:
                                gpt2_text = gpt2_text[newline_ind+1:]
                        except ValueError:
                            print(C.RED + gpt2_text + C.ENDC + "[END]")
                            print(C.YELLOW + "Exception: Invalid chat history")
                            valid_context = True                
                
                # re-encode fixed chat history
                context_tokens = enc.encode(gpt2_text)[-config.history_length+config.max_response_length:]
                if config.debug:
                    print(context_tokens)
                    print(C.BLUE + enc.decode(context_tokens) + C.ENDC + "[END]")
                
                #annoying
                #if len(context_tokens) < 100 and not config.quiet:
                #    print(C.YELLOW + "Warning: only given {} tokens to generate from, output may be low-quality.".format(len(context_tokens)) + C.ENDC)
                
                if adventure:
                    choice = 0
                    while choice == 0:
                        potential_outputs = {}
                        for k in range(config.choices):
                            # prevent thought loops
                            output = last_response
                            while thought_loop_check(output, last_response, prev_last_response, user_input, config.response_distance) or output.lstrip() not in potential_outputs:
                                temperature = temperature_curve_sample(config.min_temperature, config.max_temperature, config.temperature_curve)
                                if config.debug:
                                    print(C.BLUE + f"Temperature: {temperature:.2f} + {additional_temperature:.2f}" + C.ENDC)
                                    print(C.BLUE + enc.decode(context_tokens[-config.history_length+config.max_response_length:]) + C.ENDC)
                                out = sample_sequence(
                                    model=model, length=config.max_response_length,
                                    context=context_tokens[-config.history_length+config.max_response_length:] if not config.unconditional else None,
                                    start_token=enc.encoder["<|endoftext|>"] if config.unconditional else None,
                                    end_tokens=end_tokens,
                                    batch_size=1,
                                    temperature=temperature,
                                    top_k=config.top_k,
                                    top_p=config.top_p,
                                    device=device,
                                    verbose=config.verbose
                                )
                                out = out[:, len(context_tokens):].tolist()
                                output = enc.decode(out[0]).replace("\r", "").rstrip()
                                #if output.lstrip() not in outputs and output.lstrip() not in potential_outputs:
                                if output not in potential_outputs and not thought_loop_check(output, last_response, prev_last_response, user_input, config.response_distance):
                                    potential_outputs[output] = 1
                                    break
                                if config.debug: print(C.YELLOW + 'Looping...' + C.ENDC)
                                additional_temperature += config.temperature_step
                                if temperature + additional_temperature > 2 * config.max_temperature:
                                    if config.verbose: print(C.YELLOW + "Model output stuck repeating. Choosing random response." + C.ENDC)
                                    output = " " + random.choice(random_responses)
                                    potential_outputs[output] = 1
                                    additional_temperature = 0 # reset
                                    break
                                if config.debug:
                                    print(C.BLUE + prev_last_response + C.ENDC + f" {len(prev_last_response)}")
                                    print(C.BLUE + last_response + C.ENDC + f" {len(last_response)}")
                                    print(C.BLUE + output + C.ENDC + f" {len(output)}")
                        pi = 1
                        potentials = list(potential_outputs.keys())
                        for p in potentials:
                            #if finish_sentence:
                            #    p = " "+p
                            if init == "":
                                print(f"{pi}:{p}".format(pi, init+p))
                            else:
                                print(f"{pi}: {init}{p}".format(pi, init+p))
                            pi += 1
                        user_choice = input('Which one? ')
                        if user_choice == '':
                            choice = 0
                            gpt2_text = original_gpt2_text + prefix
                            init = ""
                            # need to re-encode
                            context_tokens = enc.encode(gpt2_text)[-config.history_length+config.max_response_length:]
                            gpt2_text = enc.decode(context_tokens)
                        elif ord(user_choice[0]) >= ord('0') and ord(user_choice[0]) <= ord('9'):
                            choice = min(config.choices, max(0, int(user_choice)))
                        else:
                            choice = 0
                            if config.debug:
                                print(C.BLUE + original_gpt2_text + C.ENDC)
                                print(C.BLUE + prefix + C.ENDC)
                                print(C.BLUE + user_choice + C.ENDC)
                                print(C.BLUE + init + C.ENDC)
                            gpt2_text = original_gpt2_text + prefix + " " + user_choice
                            if config.debug:
                                print(C.YELLOW + gpt2_text + C.ENDC)
                            init = user_choice
                            # need to re-encode
                            context_tokens = enc.encode(gpt2_text)[-config.history_length+config.max_response_length:]
                            gpt2_text = enc.decode(context_tokens)
                        additional_temperature = max(0, additional_temperature - config.choices * config.temperature_step) * config.temperature_decay
                    prev_last_response = last_response
                    last_response = output                    
                    #if init == "":
                    #    output = potentials[choice-1]
                    #else:
                    output = potentials[choice-1]
                    #outputs[output.lstrip()] = 1
                    #if config.debug:
                    #    print(C.BLUE + repr(outputs) + C.ENDC)
                    #adventure = False
                else:
                    #for i in range(config.choices): #this should be a function :^)
                    temperature = temperature_curve_sample(config.min_temperature, config.max_temperature, config.temperature_curve)
                    output = last_response
                    while thought_loop_check(output, last_response, prev_last_response, user_input, config.response_distance):
                        if config.debug: print(C.BLUE + f"Temperature: {temperature:.2f} + {additional_temperature:.2f}" + C.ENDC)
                        out = sample_sequence(
                            model=model, length=config.max_response_length,
                            context=context_tokens[-config.history_length+config.max_response_length:] if not config.unconditional else None,
                            start_token=enc.encoder["<|endoftext|>"] if config.unconditional else None,
                            end_tokens=end_tokens,
                            batch_size=1,
                            temperature=temperature + additional_temperature,
                            top_k=config.top_k,
                            top_p=config.top_p,
                            device=device,
                            verbose=config.verbose
                        )
                        out = out[:, len(context_tokens):].tolist()
                        output = enc.decode(out[0]).replace("\r", "").rstrip()
                        additional_temperature += config.temperature_step
                        if temperature + additional_temperature > 2 * config.max_temperature:
                            if config.verbose: print(C.YELLOW + "Model output stuck repeating. Choosing random response." + C.ENDC)
                            output = " " + random.choice(random_responses)
                            additional_temperature = 0 # reset
                            break
                        if config.debug:
                            print(C.BLUE + prev_last_response + C.ENDC + f" {len(prev_last_response)}")
                            print(C.BLUE + last_response + C.ENDC + f" {len(last_response)}")
                            print(C.BLUE + output + C.ENDC + f" {len(output)}")
                            print(C.BLUE + "Distance:", Levenshtein.distance(output, last_response), C.ENDC)
                    assert output != last_response
                    additional_temperature = max(0, additional_temperature - config.temperature_step) * config.temperature_decay
                    prev_last_response = last_response
                    last_response = output
                    # if config.debug:
                    #     for i in range(1,len(out[0])):
                    #         print(out[0][0:i])
                    #         print(C.BLUE + enc.decode(out[0][0:i]) + C.ENDC + "[END]")
                    # if output.lstrip() not in outputs:
                    #     outputs[output.lstrip()] = 1
                    #     break
                    #if config.debug:
                    #    print(C.YELLOW + "Looping..." + C.ENDC)
                eot = min(output.rfind("<|endoftext|>"), output.rfind(f"{config.gpt2_name}:"), output.rfind(f"{config.gpt2_waifu}:"))
                if eot >= 0:
                    output = output[:eot]
                gpt2_text += output + "\n"
                if init != "": init = " " + init
                
                if not finish_sentence:
                    if config.rp:
                        wrap_print(init + output, config.wrap_width)
                    else:
                        wrap_print(C.WAIFU + config.display_waifu + C.ENDC + ":" + init + output, config.wrap_width)
                    
                    chatlog.write(config.display_waifu + ":" + init + output + "\n")
                else:
                    if config.rp:
                        wrap_print("..." + output, config.wrap_width)
                    else:
                        wrap_print(C.USER + config.display_name + C.ENDC + ": ..." + output, config.wrap_width)
                    chatlog.write(output + "\n")
                if init != "": init = ""
    chatlog.close()

if __name__ == "__main__":
    from argparse import ArgumentParser
    import json
    parser = ArgumentParser()
    parser.add_argument("--prompt", help="initial chat message")
    parser.add_argument("--init", help="initial waifu chat message", default="")
    parser.add_argument("--display-name", help="your name", default=DEFAULT["display_name"])
    parser.add_argument("--display-waifu", help="your waifu's name", default=DEFAULT["display_waifu"])
    parser.add_argument("--seed", help="background text to seed GPT-2 generation from")
    parser.add_argument("--chattiness", type=int, help="how many lines of responses to generate", default=DEFAULT["chattiness"])
    parser.add_argument("--gpt2-name", help="your gpt-2 name", default=DEFAULT["gpt2_name"])
    parser.add_argument("--gpt2-waifu", help="your waifu's gpt-2 name", default=DEFAULT["gpt2_waifu"])
    parser.add_argument("--wrap-width", "-w", type=int, default=DEFAULT["wrap_width"],
        help="wrap text to character width")
    parser.add_argument("--model", help="gpt2 model", default=DEFAULT["model"],
        choices=["distilgpt2", "gpt2", "gpt2-medium", "gpt2-large", "gpt2-xl"])
    parser.add_argument("--verbose", "-v", action="store_true", help="print arguments")
    parser.add_argument("--debug", "-d", action="store_true", help="debugging information")
    parser.add_argument('--unconditional', action='store_true',
        help="if true, unconditional generation")
    parser.add_argument("--history-length", type=int, default=DEFAULT["history_length"],
        help="maximum chat context history length in gpt-2 tokens")
    parser.add_argument("--max-response-length", type=int, default=DEFAULT["max_response_length"],
        help="Maximum amount of tokens that can be generated for a response")
    parser.add_argument("--min-temperature", "-t", type=float, default=DEFAULT["min_temperature"])
    parser.add_argument("--max-temperature", "-m", type=float, default=DEFAULT["max_temperature"])
    parser.add_argument("--temperature-step", type=float, default=DEFAULT["temperature_step"],
        help="Temperature automatically increases by this step size whenever the model loops")
    parser.add_argument("--temperature-decay", type=float, default=DEFAULT["temperature_decay"],
        help="This controls the decay of added temperature caused by looping back to normal.")
    parser.add_argument("--temperature-curve", "-c", type=float, default=DEFAULT["temperature_curve"],
        help="temperature exponent factor of the random temperature sampling")
    parser.add_argument("--response-distance", type=int, default=DEFAULT["response_distance"],
            help="Ensure that responses have at least given Levenshtein distance from recent ones")
    parser.add_argument("--choices", type=int, default=DEFAULT["choices"],
        help="number of choices in adventure mode")
    parser.add_argument("--top-k", type=int, default=DEFAULT["top_k"])
    parser.add_argument("--top-p", type=float, default=DEFAULT["top_p"])
    parser.add_argument("--log", type=bool, default=True, help="save chat log")
    parser.add_argument("--gpu", type=int, default=DEFAULT["gpu"], help="GPU device")
    parser.add_argument("--rp", default=DEFAULT["rp"], action="store_true",
        help="Role playing mode, no names")
    parser.add_argument("--adventure", default=DEFAULT["adventure"], action="store_true",
        help="Choose your own adventure mode, provides choices")
    #parser.add_argument("--quiet", action="store_true", help="suppress warnings")
    # parser.add_argument("--action-copycat", type=float, default=DEFAULT["action_copycat"],
    #     help="Chance of forcing an action response if user input starts or ends with *do something*")
    parser.add_argument("--no-colors", action="store_true",
        help="For legacy systems: if there are strange characters in output, disable the colors.")
    args = parser.parse_args()
    config = Config(DEFAULT)
    config.update(json.load(open("config.json")))
    args_dict = dict(args.__dict__)
    for key in list(args_dict):
        if args_dict[key] == DEFAULT[key]:
            del args_dict[key]
    config.update(args_dict)
    try:
        main(config)
    except KeyboardInterrupt:
        if config.no_colors:
            C = NoColors
        else:
            C = Colors
        goodbye = random.choice(["Goodbye", "Talk to you later"])
        print(f"\n{C.WAIFU}{config.display_waifu}{C.ENDC}: {goodbye}")
